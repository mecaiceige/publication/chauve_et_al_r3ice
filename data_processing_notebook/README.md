# Run the notebook

## Install `base_cti` environment

1. Follow the procedure [here](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/tree/main/cti_python?ref_type=heads) to have a `base_cti` environment

2. add additionary dependency

```bash
mamba activate base_cti
mamba install vtk
pip install xarray-symTensor2d
```