# Run $R^3iCe$ simulation

```
time mpirun -n 16 R3iCe -j metadata.json 2> /dev/null
```
