# Simulation

This folder provides all the input file in order to re-run the simulation. [$R^3iCe$ v0.8.4](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/tree/0.8.4?ref_type=tags) was used in this paper.