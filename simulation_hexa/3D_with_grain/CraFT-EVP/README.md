# Run $craft$ simulation

```
mkdir output
cd output/
time craft -v -n 32 -f ../n500.in
```
