# Chauve et al. 2024

[![doi](https://zenodo.org/badge/doi/10.5802/crmeca.243.svg)](https://doi.org/10.5802/crmeca.243)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](LICENCE)

**Title:** A physically-based formulation for texture evolution during dynamic recrystallization. A case study of ice.

**Authors:** Thomas Chauve, Maurine Montagnat, Véronique Dansereau, Pierre Saramito, Kévin Fourteau, and Andréa Tommasi

**Journal:** [Comptes Rendus Mécanique](https://comptes-rendus.academie-sciences.fr/mecanique/)

<img src="logo_R3iCe_crmeca.png" alt="R3iCe" width="100"/>

## Versions

- **0.1**: first draft to all co-authors
- **0.2**: last [JTCAM](https://jtcam.episciences.org/) before change to [Comptes Rendus Mécanique](https://comptes-rendus.academie-sciences.fr/mecanique/)
- **0.3**: second version to all co-authors
- **0.4**: submitted to *Compte Rendus Mécanique* [submitted version](https://hal.science/hal-04231338v1)
- **0.5**: 1$^{st}$ review [reviewed version](https://hal.science/hal-04231338v2)
- **0.6**: fix typo thanks to $2^{nd}$ review
- **0.7**: first round of proof
- **1.0**: publish version

